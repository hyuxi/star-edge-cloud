package config

// Config -配置
type Config struct {
	Port           string
	MetadataDBPath string
	LogDBPath      string
}
