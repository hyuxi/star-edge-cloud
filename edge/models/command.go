package models

// Command -
type Command struct {
	ID   string
	Type string
	Data []byte
}
